#!/bin/bash
apt -y apt update && apt -y upgrade && apt -y dist-upgrade
apt -y install vim openssh-server
cat loghost.pub >> /root/.ssh/authorized_keys
FILE="v109";
echo -e "$(cat /sys/class/net/eno1/address | tr : - )$(hostname)\n$(ip address)\n\n" >> $FILE
