#!/bin/bash

# Program made to automaticaly extract documentation into an markdown file:

#TONGUE=$1

	if [ -n "$TONGUE" ]; then
	if ! [ -d "$TONGUE" ]; then
		echo -e "Missing language";
		exit
	fi
else
	TONGUE=en
fi

INFILE="$1" # Input file

OUTFILE="$2" # Output file

grep '#' $INFILE |\
# Leave the titles with "#", and break line
sed s/"## "/"\n## "/ |\
# Removes the '\t', so it turns into text only
sed s/"#\t"/""/ |\
# TODO: Find a way to remove '#' when it's alone in the line
sed s/\#\r\|\n// |\
# Remove remainings # in one line alone
sed s/"#"/""/ |\
# Insert a space in * so text dont turn into italic
sed s/\*/" \* "/g \
> $OUTFILE

cp $OUTFILE README.md

#{
# Count how many lines the file has
lines=$(wc -l $INFILE | cut -d ' ' -f 1)

# Initialize the file i want to use
echo ";lettuce;"$(\
	sed "1!d" $INFILE |\
	sed 's/\t\t\t\t/;potato;/g' |\
	sed 's/\t\t\t/;avocado;/g' |\
	sed 's/\t\t/;strawberry;/g' |\
	sed 's/\t/;grape;/g' |\
	sed 's/     /;sapphire;/g' |\
	sed 's/    /;emerald;/g' |\
	sed 's/   /;ruby;/g' |\
	sed 's/  /;onix;/g' ) > /tmp/buffer
#
## Go through each line
for i in $(seq 2 $lines); do
	echo ";lettuce;"$(\
	sed "$i!d" $INFILE |\
	sed 's/\t\t\t\t/;potato;/g' |\
	sed 's/\t\t\t/;avocado;/g' |\
	sed 's/\t\t/;strawberry;/g' |\
	sed 's/\t/;grape;/g' |\
	sed 's/     /;sapphire;/g' |\
	sed 's/    /;emerald;/g' |\
	sed 's/   /;ruby;/g' |\
	sed 's/  /;onix;/g' ) >> /tmp/buffer
#
#	# Lettuce and potato are marks used to keep the file formatted:
#	# lettuce keeps blank lines in place, potato keeps the tabs, and
#	# the semicollons separates the words from othes, so in Albert ;potato;
#	# Blue, for example, potato will not be considered name, and will
#	# be translated as well. Finally, saphire is used for two spaces
#
#	# If there are more than one occurance of the same char, subtitute
#	# for another "dictionary word". (TODO: find a better way to do this)
done
#
./translate/translate -shell -brief -j < /tmp/buffer > /tmp/buffer2
#
echo $(\
	sed "1!d" /tmp/buffer2 |\
	sed 's/;alface;//g' |\
	sed 's/;batata;/\t\t\t\t/g' |\
	sed 's/;abacate;/\t\t\t/g' |\
	sed 's/;morango;/\t\t/g' |\
	sed 's/\t/;uva;/g' |\
	sed 's/;safira;/     /g' |\
	sed 's/;esmeralda;/    /g' |\
	sed 's/;rubi;/   /g' |\
	sed 's/;onix;/  /g' |\
	sed 's/;potato;/\t\t\t\t/g' |\
	sed 's/;avocado;/\t\t\t/g' |\
	sed 's/;strawberry;/\t\t/g' |\
	sed 's/;grape;/\t/g' |\
	sed 's/;sapphire;/     /g' |\
	sed 's/;emerald;/    /g' |\
	sed 's/;ruby;/   /g' |\
	sed 's/u003/=/g' ) > $OUTFILE
#
for i in $(seq 2 $lines); do
## Translating batata into \t and alface in nothing will revert the file to
## it's original format
	echo $(\
		sed "$i!d" /tmp/buffer2 |\
		sed 's/;alface;//g' |\
		sed 's/;batata;/\t\t\t\t/g' |\
		sed 's/;abacate;/\t\t\t/g' |\
		sed 's/;morango;/\t\t/g' |\
		sed 's/;uva;/\t/g' |\
		sed 's/;safira;/     /g' |\
		sed 's/;esmeralda;/    /g' |\
		sed 's/;rubi;/   /g' |\
		sed 's/;onix;/  /g' |\
		sed 's/;lettuce;//g' |\
		sed 's/;potato;/\t\t\t\t/g' |\
		sed 's/;avocado;/\t\t\t/g' |\
		sed 's/;strawberry;/\t\t/g' |\
		sed 's/;grape;/\t/g' |\
		sed 's/;sapphire;/     /g' |\
		sed 's/;emerald;/    /g' |\
		sed 's/;ruby;/   /g' |\
		sed 's/u003/=/g' ) >> $OUTFILE
#
## The last sed is used to translate back the original arrow symbol: gtrans
## turns into a weird code
done
#
